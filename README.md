#(How to) Configurar el módulo MP3
================================================

Si necesitas reproducir un fichero MP3 en tu aplicación de forma sencilla este es tu módulo. 
El funcionamiento del mismo es muy sencillo: Recupera un fichero MP3 de internet, lo descarga y 
lo reproduce de forma automática o no.

El módulo mp3, como todos los demás está **disponible en nuestro market** (y es gratis). Así que sólo hay que seleccionarlo y añadirlo al editor de la App. Como en el resto de módulos, **la configuración es sencilla** pues sólo nos reclama rellenar tres campos. Como es natural, podremos cambiarle el nombre al módulo para que al usuario le resulte más descriptivo en el menú. Los campos necesarios son los siguientes:

  - **title**: El título de la pista a mostrar. Este campo es opcional por lo que si no se define o se define vacío, no se mostrará nada en el título.
  
  - **mp3**: La url completa del mp3 a reproducir.
  
  - **bpm**: Los latidos por minuto del reproductor. En caso de que valga 0 no se incluye este efecto.
