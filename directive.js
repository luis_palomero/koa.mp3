angular.directive('mp3Tag', [function(){
  return {
    restrict: 'E',
    link: function(scope, element, attrs){
      var mp3Url = scope.mp3.modulescope.mp3;

      var bpm = scope.mp3.modulescope.bpm;

      element.html('<audio loop="loop" data-bpm="'+bpm+'"><source src="' + mp3Url + '" type="audio/mp3" >Your browser does not support the audio element.</audio>');

    }


  }
}]);
