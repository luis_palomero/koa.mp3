
angular
  .controller('mp3Controller', loadFunction);

loadFunction.$inject = ['$scope','$rootScope', '$http', 'structureService', '$location'];

function loadFunction($scope, $rootScope, $http, structureService, $location){

  structureService.registerModule($location, $scope, "mp3");
  $scope.title = $scope.mp3.modulescope.title;

  $scope.play = function(){
    var audio = $('audio')[0];
    var spinner = $('.spinner-wrap');
    var bpm = Math.max(Number($('audio').data('bpm')), 0);

    var pulse = (bpm)?(60/bpm)*1000:0

    if(audio.paused === false){
      audio.pause();
      audio.currentTime = 0;
      spinner.removeClass('playing');
      if(pulse){
        clearInterval(pulseInterval);
      }
    } else {
      audio.play();
      spinner.addClass('playing');
      if(pulse){
        pulsing();
        pulseInterval = setInterval(function(){pulsing()},pulse);
      }
    }

    function pulsing(){
      spinner.addClass('pulse');
      setTimeout(function(){
        spinner.removeClass('pulse');
      }, pulse - 100);
    }

  }
}
